# Whiteboard Content Scanner

Image Processing - SCC0251

Final Project

Authors:
* Rodrigo Mendes Andrade (10262721)
* Marcelo Kiochi Hatanaka (10295645)
* Lucas Nobuyuki Takahashi (10295670)

Folders and Files:
* [Python Code](/python_code) contains the Python code used for the application
* [Images](/images) contains images used in the demos
* [Jupyter Notebook](dip_final_project_presentation.ipynb) contains a demo executing a small example of the algorithm

**Main goal**

The goal of this project is to modify images, colorized , generated from whiteboard photos, in order to improve visualization and legibility of its written/drawn content. This is done by image enhancement, filtering and restoration processes such as denoising and deblurring and may be used to digitize whiteboard contents.

The main struggle is when there’s low contrast between the background and the whiteboard. There are 3 approaches being considered for this:
- Identifying and cutting only the region corresponding to the whiteboard to be processed (main approach);
- Doing nothing and letting the algorithm process non-interesting areas;
- Adding an interface on which the user can define the area of the whiteboard to be processed.

Another difficulty to be considered is that the stroke intensity of the whiteboard content may be low, which should be solved by the image processing techniques cited above. The output image should consist of the whiteboard drawn/written content in vivid tones (of its original colors) against a white background.

**Input Images**

The input images consist of whiteboard pictures (with drawings), taken by the authors of the project from smartphones, in a variety of test case scenarios such as: varying light intensity scenarios, varying background contrast, noisy and blurred images. The photos are taken with an approximately 90º angle with the whiteboard.

**Steps to reach the goal**

The steps to reach the goal are:
1. Remove any image noise that may interfere on the rest of the processing (image denoising)
1. Remove any image blur (image deblurring)
1. Isolate the whiteboard by removing the image’s background (image segmentation)
1. Increase contrast between the drawings and the whiteboard by uniformizing the whiteboard color intensity
1. Improve the colors of the drawings, increasing brightness and saturation (image and color enhancement)

**Presentation video on youtube**

https://www.youtube.com/watch?v=6-ctdyjrwHY&feature=youtu.be