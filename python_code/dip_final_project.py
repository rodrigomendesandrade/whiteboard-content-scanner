import numpy as np
import matplotlib as mpl
import imageio
from scipy import ndimage
from scipy.fftpack import fftn, ifftn, fftshift
import argparse

parser = argparse.ArgumentParser(description="The goal of this program is to modify images, colorized , generated from whiteboard photos, in order to improve visualization and legibility of its written/drawn content. This is done by image enhancement, filtering and restoration processes such as denoising and deblurring and may be used to digitize whiteboard contents.")

parser.add_argument('image_input' , help="the image that will be used in the program")
parser.add_argument('-o' , metavar='file_name', help='the output name of the image' , default="output.png")

args = parser.parse_args()

def contrast_modulation(input_image , c , d):
    """ this function do the contrast modulation
    """
    min_image_input_value = np.amin(input_image)
    max_image_input_value = np.amax(input_image)
    aux_image = input_image.astype('uint32') # this conversion is to prevent overflow
    return (aux_image - min_image_input_value) * (d-c)/(max_image_input_value - min_image_input_value) + c

def filter_padding( image_shape , filter_shape ):
    """
        return a tuple for numpy.pad

        Parameters
        ----------

        image_shape : tuple
            the image shape to calculate the pad sizes
        filter_shape : tuple
            the filter shape to calculate the pad sizes
    """
    # for even numbers
    aux01 = image_shape[0] - filter_shape[0]
    aux02 = image_shape[1] - filter_shape[1]
    half_aux01 = aux01 // 2
    half_aux02 = aux02 // 2
    return ((aux01 - half_aux01 , half_aux01) , (aux02 - half_aux02 , half_aux02))

def gaussian_filter(k=5, sigma=1.0):
    ''' Gaussian filter
    :param k: defines the lateral size of the kernel/filter, default 5
    :param sigma: standard deviation (dispersion) of the Gaussian distribution
    :return matrix with a filter [k x k] to be used in convolution operations
    '''
    arx = np.arange((-k // 2) + 1.0, (k // 2) + 1.0)
    x, y = np.meshgrid(arx, arx)
    filt = np.exp( -(1/2)*(np.square(x) + np.square(y))/np.square(sigma) )
    return filt/np.sum(filt)

def CLSF(image , gamma , gaussian_filter):
    """
        function to execute Constrained least square filtering in the passed image

        Parameters
        ----------

        image : imageio.core.util.Array , numpy.ndarray
            image with will be used in the algorithm
        gamma : float64
            the gamma value in the algorithm
        gaussian_filter : numpy.ndarray
            the gaussian kernel
    """

    hp = np.pad(gaussian_filter, filter_padding(image.shape , gaussian_filter.shape), "constant",  constant_values=0)

    # generate the laplacian filter with padding
    p = np.array([0,-1,0,-1,4,-1,0,-1,0]).reshape((3,3))
    pp = np.pad(p, filter_padding(image.shape , p.shape), "constant",  constant_values=0)

    H = fftn(hp)
    HC = np.conj(H)
    G = fftn(image)
    P = fftn(pp)

    F = np.multiply((HC/(np.square(np.abs(H)) + gamma*np.square(np.abs(P)))),G)

    return np.real(fftshift(ifftn(F)))

def fft_imagefilter(g, w):
    ''' A function to filter an image g with the filter w
    '''
    wp = np.pad(w, filter_padding(g.shape , w.shape), "constant",  constant_values=0)

    # computing the Fourier transforms
    W = fftn(wp)
    G = fftn(g)
    R = np.multiply(W,G)

    r = np.real(fftshift(ifftn(R)))
    return r


def denoising_2(kernel , image):
    """
    auxiliary function to denoising and deblur

    Parameters
    ----------

    kernel : numpy.ndarray
    The gaussian kernel which will be used in the gaussian blur, and in the Constrained Least Square filter

    image : imageio.core.util.Array , numpy.ndarray
    The image which will be used in the function
    """
    max_gray_value = np.amax(image)

    # denoising

    denoising = fft_imagefilter(image , kernel)

    denoising = contrast_modulation(denoising, 0 , max_gray_value)
    max_gray_value = np.amax(denoising)

    deblur = CLSF(denoising , 0.5 , kernel)
    deblur = contrast_modulation(deblur , 0 , max_gray_value).astype(np.uint8)

    return deblur

def denoising(kernel , image):
    """
        Function to do the denoising and deblur in a RGB image
        Parameters
        ----------

        kernel : numpy.ndarray
        The gaussian kernel which will be used in the gaussian blur, and in the Constrained Least Square filter

        image : imageio.core.util.Array , numpy.ndarray
        The image which will be used in the function

        Return
        ------
            numpy.ndarray as an RGB image denoised and deblured
    """
    output = np.empty(image.shape)
    output[:,:,0] = denoising_2(kernel , image[:,:,0])
    output[:,:,1] = denoising_2(kernel , image[:,:,1])
    output[:,:,2] = denoising_2(kernel , image[:,:,2])

    return output.astype(np.uint8)

def gamma_enhancement(img, gamma):
    """
    function to do the gamma enhancement

    Parameters
    ----------

    img : imageio.core.util.Array , numpy.ndarray
    Image which will used in the filter

    gamma : doouble
    the exponent in the filter

    Return
    ------

    numpy.ndarray with shape of img and the result of the gamma_enhancment
    """
    R = 255
    output = np.copy(img)
    output = (R * np.power(img/R , gamma)).astype(np.uint8)
    return output

def thresholding(f, L):
    """
    Function to do a thresholding of the elements in the image f greater or equal than L

    Parameters
    ----------

    f : imageio.core.util.Array , numpy.ndarray
    Image

    L : int,float64 , float32
    Value to be used in the comparation

    return
    ------

    A new numpy.ndarray with the shape of f and values 0 if the pixel value in f is less than L otherwise 1
    """
    # create a new image with zeros
    f_tr = np.ones(f.shape).astype(np.uint8)
    # setting to 1 the pixels above the threshold
    f_tr[np.where(f < L)] = 0
    return f_tr

def var_histogram(histo):
    """
    Function to calculate the variance using a histogram

    Parameters
    ----------

    histo : numpy.ndarray
    Histogram

    Return
    ------
    the variance in the histogram
    """
    values = np.arange(0,histo.size)
    mean = np.average(values , weights=histo);
    aux = values - mean
    var = np.sum(aux * aux * histo)/ np.sum(histo)
    return var

def otsu_threshold(img, max_L):
    """
    Function to do the otsu threshold

    Parameters
    ----------

    img : imageio.core.util.Array , numpy.ndarray
    the image which will be used in the function

    max_L : int
    max value that will be tested in the function

    return
    ------

    the function return two values:
    the first value is a threshold of the img, and the second onw is the value used to do the threshold
    """
    M = np.product(img.shape)
    min_var = 1e9
    indice = -1
    hist_t,a = np.histogram(img, bins=256, range=(0,256))
    for L in np.arange(1, max_L):
        # computing weights
        hist1 = hist_t[:L]
        hist2 = hist_t[L:]
        w_a = np.sum(hist1)/float(M)
        w_b = 1 - w_a
        # computing variances
        if( w_a != 0 and w_b != 0):
            sig_a = var_histogram(hist1)
            sig_b = var_histogram(hist2)
            aux =  w_a*sig_a + w_b*sig_b
            if(min_var > aux):
                min_var = aux
                indice = L
    img_t = thresholding(img, indice)
    return img_t, indice

def remove_background(img):
    """
        This function remove the background of the image trying to leave only the whiteboard

        Parameters
        ----------

        img is the image with the whiteboard

        Return
        ------

        return a image with the whiteboard with the background removed fully or partial
    """

    # transform in grayscale image, correct contrast and brightenes
    gray = np.sum(img, axis=2)/3
    gray = contrast_modulation(gray , 0 , 255).astype(np.uint8)
    gray = gamma_enhancement(gray , 1.5)

    #Otsu Threshold
    img_t , OL = otsu_threshold(gray , 256)

    #Dilating to include the draw in the mask
    square = np.ones((30,30) , dtype=np.bool)
    img_dilation = ndimage.binary_dilation(img_t , structure=square)


    #erosing to remove the border created by the previo step
    img_dilation = ndimage.binary_erosion(img_dilation , structure=square)

    square = np.ones((10,10) , dtype=np.bool)
    img_ero = ndimage.binary_erosion( img_dilation , iterations=17 ,  structure=square)

    img_out = ndimage.binary_dilation(img_ero , iterations=17 ,  structure=square)

    output = np.empty(img.shape,dtype=np.uint8)
    output[np.logical_not(img_out)] = 255
    output[img_out] = img[img_out]

    non_zeros = np.nonzero(img_out)

    if(non_zeros[0].size == 0):
        return output
    else:
        most_left = np.amin(non_zeros[1])
        most_right = np.amax(non_zeros[1])
        most_bottom = np.amax(non_zeros[0])
        most_top = np.amin(non_zeros[0])
        return output[most_top:most_bottom , most_left:most_right]

def normalize_minmax(f, factor):
    """
        This function normalizes the values of an array between 0 and a given value

        Parameters
        ----------

        f is the array to be normalized

        factor is the maximum value of the array after the normalization

        Return
        ------

        Returns the normalized array
    """
    f_min = np.min(f)
    f_max = np.max(f)
    f = (f - f_min)/(f_max - f_min)
    return (f * factor)

def luminance(img):
    """
        This function takes an RGB image and returns its grayscale version

        Parameters
        ----------

        img is the image to be converted

        Return
        ------

        Returns the grayscale version of the image
    """
    img = np.array(img, copy=True).astype(float)

    # generate grayscale image from a weighted mean of the 3 RGB channels
    new_img = np.zeros((img.shape[0], img.shape[1]))
    new_img = img[:,:,0]*0.299 + img[:,:,1]*0.587 + img[:,:,2]*0.114
    new_img = normalize_minmax(new_img, 255)
    return new_img

def get_middle(hist):
    """
        get the middle value of the histogramn, using as a start the first non zero element and the 
        last non zero element as end.

        Parameters
        ----------

        hist : numpy.ndarray
            Histogram used in the function

        Return
        ------
        a int representing the midle of the histogram
    """
    min = 0
    max = 253

    while hist[min] == 0:
        min = min + 1
    while hist[max] == 0:
        max = max - 1
    
    return int((max + min)/2)

def define_threshold(hist, T):
    """
        This function defines a new threshold for the histogram by taking the
        mean values of the 2 groups defined by the previous threshold and calculating
        their mean

        Parameters
        ----------

        hist is the histogram to be segmented

        T is the previous threshold

        Return
        ------

        Returns a new threshold
    """
    hist1 = hist[:T]
    hist2 = hist[T:254]
    
    values1 = np.arange(0, T, 1)
    values2 = np.arange(T, 254, 1)

    m1 = np.sum(hist1*values1)/np.sum(hist1)
    m2 = np.sum(hist2*values2)/np.sum(hist2)

    return ((m1+m2)/2).astype(np.uint8)

def contrast_threshold(hist, initial_t):
    T1 = initial_t
    T2 = 1
    while T1 != T2:
        T2 = T1
        T1 =  define_threshold(hist, T1)
    
    return T1

def contrast(img, c, m):
    new_img = (255/(1+np.exp(-c*(img.astype(np.float64)-m)))).astype(np.uint8)
    return new_img
  
def enhance_strokes(image , hist , c ):
    initial_t = get_middle(hist)
    t = contrast_threshold(hist, initial_t)
    return contrast(image, c, t)

def main():
    original_img = imageio.imread(args.image_input)
    kernel = gaussian_filter(11, 0.8)

    dndb_img = denoising(kernel, original_img)
    cut_img = remove_background(dndb_img)
    lum_img = luminance(cut_img)
    hist,_ = np.histogram(lum_img, bins=255, range=(0, 255))
    filtered_img = enhance_strokes(cut_img , hist , 0.4)
    imageio.imwrite(args.o , filtered_img)

main()
